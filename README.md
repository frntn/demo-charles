# frntn/demo-charles

Get repo

```
git clone git@gitlab.com:frntn/demo-charles.git
cd demo-charles
```

Build image

```
docker build -t mynamespace/appx .
```

Start image

```
docker run mynamespace/appx
```

You'll get a message to properly start the container ;)

From within the container check the `file.jar` file exists in `/var/www/java` folder.

WOOT \o/
