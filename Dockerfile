FROM java:8-jdk
MAINTAINER fronton@ekino.com

# exec some commands with RUN keyword
RUN apt-get update && apt-get install -y vim
RUN mkdir -p /var/www/java

# include file inside the image with ADD or COPY keyword
ADD file.jar  /var/www/java

CMD ["echo", "-e", "Start me with the following:\n\n  docker run -ti --rm --name demo-charles mynamespace/appx bash\n"]